<?xml version='1.0'?>
<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="excerpt"
  base-template="standard"
  provide-type="none"
  has-homepage="false"
  category="subdocs"
  context-provider="de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSubdocumentContextProvider">
  <resource-bundle>
    <l10n>
      <name>Excerpt</name>
      <description>
        Provide an excerpt from a resource.
      </description>
      <about>
        Provide excerpts as abstracts of information found in a resource (such
        as a book). If you want to go into more detail for a given resource,
        there may be multiple excerpt documents as subpages of a resource
        document.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Exzerpte">Exzerpt</name>
      <description>
        Geben Sie ein Exzerpt für eine Ressource an.
      </description>
      <about>
        Verwenden Sie Exzerpte um Informationen aus einer Ressource
        zusammenzufassen.
      </about>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.name">
      <value><xml><![CDATA[<at:var at:name="projectdoc_doctype_common_name"/>]]></xml></value>
      <controls>hide</controls>
    </property>
    <property key="projectdoc.doctype.common.parent">
      <value>
        <macro name="projectdoc-transclusion-parent-property">
          <param
            name="property-name"
            key="projectdoc.doctype.common.name" />
          <param
            name="property"
            key="projectdoc.doctype.common.parent" />
          <param name="parent-doctype">#ANY</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.excerpt.authors">
      <value>
        <macro name="projectdoc-transclusion-parent-property">
          <param name="property-name" key="projectdoc.doctype.excerpt.authors" />
          <param name="parent-doctype">#ANY</param>
          <param name="property" key="projectdoc.doctype.excerpt.authors" />
          <param name="add-link">false</param>
        </macro>
      </value>
      <controls>hide</controls>
      <resource-bundle>
        <!-- Must be the same label as for Resource. -->
        <l10n>
          <name>Authors</name>
        </l10n>
        <l10n locale="de">
          <name>Autoren</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.sortKey">
      <value>
        <xml><![CDATA[<at:var at:name="projectdoc_doctype_common_sortKey"/>]]></xml>
      </value>
      <controls>hide</controls>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Provide a short description about the content of the excerpt. Add
            the excerpt sections after this description.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Geben Sie eine kurze Beschreibung des Inhalts dieses Auszugs an.
            Abschnitte aus dem Originaldokument werden im Anschluss dieses
            Abschnitts wiedergegeben.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="resource" />
    <doctype-ref id="quote" />
    <doctype-ref id="docsection" />
    <doctype-ref id="section" />
    <doctype-ref id="generic" />
  </related-doctypes>

  <wizard template="no-homepage">
    <param name="adjust-title-on-any-parent">true</param>
  </wizard>
</doctype>
