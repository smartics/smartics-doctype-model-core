<?xml version='1.0'?>
<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="generic"
  base-template="standard"
  provide-type="none"
  has-homepage="false"
  category="authors">
  <resource-bundle>
    <l10n>
      <name>Generic</name>
      <description>
        Provide a non-specific document.
      </description>
      <about>
        Add a document with a non-specific doctype. Use generic documents if no
        provided document type matches and creating a new doctype for the
        project does not pay off.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Generisches Dokumente">Generisches Dokument</name>
      <description>
        Erstellen Sie ein unspezifisches Dokument.
      </description>
      <about>
        Mittels generischer Dokumente können Sie Dokumentinstanzen erzeugen,
        für die kein passender Dokumenttyp bereit gestellt wird.
      </about>
    </l10n>
  </resource-bundle>

  <sections>
    <section key="projectdoc.doctype.generic.children">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.generic.subdocuments"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">generic</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="render-classes">children-table, display-table, children</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <label key="projectdoc.doctype.generic.subdocuments">Subordinate Documents</label>
        </l10n>
        <l10n locale="de">
          <label key="projectdoc.doctype.generic.subdocuments">Untergeordnete Dokumente</label>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="association" />
    <doctype-ref id="aspect" />
    <doctype-ref id="docsection" />
    <doctype-ref id="excerpt" />
    <doctype-ref id="section" />
    <doctype-ref id="step" />
  </related-doctypes>
</doctype>
