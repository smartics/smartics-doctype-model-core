<?xml version='1.0'?>
<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="class"
  base-template="standard"
  provide-type="standard-type"
  category="index">
  <resource-bundle>
    <l10n>
      <name plural="Classes">Class</name>
      <description>
        Organize documents by generic classes.
      </description>
      <about>
        Document the semantics of a class. In opposition to tags, classes are
        not domain specific. A class is a generic classifier for all types of
        documents.
      </about>
    </l10n>
    <l10n locale="de">
      <name>Klasse</name>
      <description>
        Organisieren Sie Ihre Dokumente anhand von Klassen.
      </description>
      <about>
        Dokumentieren Sie die Bedeutung einer Klasse. Im Gegensatz zu Tags sind
        Klassen nicht spezifisch für Ihre Domäne. Eine Klasse ist somit ein
        generischer Auszeichner.
      </about>
      <type plural="Klassentypen">Klassentyp</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.name">
      <value><xml><![CDATA[<at:var at:name="projectdoc_doctype_common_name"/>]]></xml></value>
      <controls>hide</controls>
    </property>
    <property key="projectdoc.doctype.common.classification">
      <resource-bundle>
        <l10n>
          <name>Classification</name>
          <description>
            Add references to documents classified by this class. Allows to
            classify this class with documents of arbitrary types.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Klassifizierung</name>
          <description>
            Fügen Sie eine Referenz auf ein Dokument hinzu, das diese Klasse
            genauer beschreibt. Die Eigenschaft erlaubt es einer Klasse sich
            durch Dokumente eines beliebigen Typs auszuzeichnen.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">class-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
           Describe the class' semantics. Classes should be meaningful and usable
           for all documents independent on their type. In separating tags
            (domain specific) and classes (domain agnostic) it is easier to
            separate domain specific from generic terms.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie die Bedeutung der Klasse. Klassen sind für Dokumente
            jeden Typs verwendbar. Im Unterschied zu domänenspezifisch Tags
            werden Klassen domänenagonstisch verwendet.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.common.related-indices">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.common.related-indices"/></ac:parameter>
          <ac:rich-text-body>
            <ac:structured-macro ac:name="projectdoc-table-merger-macro">
              <ac:rich-text-body>
                <ac:structured-macro ac:name="projectdoc-display-table">
                  <ac:parameter ac:name="doctype">docsection</ac:parameter>
                  <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.class.template.title"/><at:i18n at:key="projectdoc.doctype.common.related-indices.constraint"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.common.name"/>}]</ac:parameter>
                  <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                  <ac:parameter ac:name="render-mode">definition</ac:parameter>
                  <ac:parameter ac:name="render-classes">related-indices-table, display-table, related-indices</ac:parameter>
                </ac:structured-macro>

                <ac:structured-macro ac:name="projectdoc-display-table">
                  <ac:parameter ac:name="doctype">docsection</ac:parameter>
                  <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                  <ac:parameter ac:name="render-mode">definition</ac:parameter>
                  <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
                  <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
                  <ac:parameter ac:name="render-classes">related-indices-children-table, display-table, related-indices</ac:parameter>
                </ac:structured-macro>
              </ac:rich-text-body>
            </ac:structured-macro>
          </ac:rich-text-body>
        </ac:structured-macro>]]></xml>
    </section>

    <section key="projectdoc.doctype.class.documents">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.class.documents"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.class.documents.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.doctype"/>|</ac:parameter>
            <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.common.classes"/>&gt; = [${<at:i18n at:key="projectdoc.doctype.common.name"/>}] NOT $&lt;<at:i18n at:key="projectdoc.doctype.common.is-a"/>&gt; = [record]</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-classes">documents-table, display-table, documents</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Documents</name>
          <intro>
            Documents in your wiki concerned with this class.
          </intro>
        </l10n>
        <l10n locale="de">
          <name>Dokumente</name>
          <intro>
            Dokumente Ihres Wikis, die dieser Klasse zugeordnet sind.
          </intro>
        </l10n>
      </resource-bundle>
    </section>
    <section key="projectdoc.doctype.class.records">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.class.records"/></ac:parameter>
        <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.class.records.intro"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="expand" >
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.class.records.expand"/></ac:parameter>
            <ac:rich-text-body>
              <p>
                <ac:structured-macro ac:name="projectdoc-display-table">
                  <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
                  <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.iteration"/>|, <at:i18n at:key="projectdoc.doctype.common.creationDate"/>|, <at:i18n at:key="projectdoc.doctype.common.lastModificationDate"/>|</ac:parameter>
                  <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.common.classes"/>&gt; = [${<at:i18n at:key="projectdoc.doctype.common.name"/>}] AND $&lt;<at:i18n at:key="projectdoc.doctype.common.is-a"/>&gt; = [record]</ac:parameter>
                  <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.creationDate"/>&#167;-</ac:parameter>
                  <ac:parameter ac:name="render-classes">records-table, display-table, records</ac:parameter>
                </ac:structured-macro>
              </p>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Records</name>
          <intro>
            Records in your wiki concerned with this class.
          </intro>
          <label key="projectdoc.doctype.class.records.expand">Expand records ...</label>
        </l10n>
        <l10n locale="de">
          <name>Aufzeichnungen</name>
          <intro>
            Aufzeichnungen in Ihrem Wiki, die dieser Klasse zugeordnet sind.
          </intro>
          <label key="projectdoc.doctype.class.records.expand">Aufzeichnungen aufklappen ...</label>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="tag" />
    <doctype-ref id="category" />
    <doctype-ref id="subject" />
  </related-doctypes>

  <wizard template="minimal-fields">
    <field template="target-location-compact"/>
    <field
      template="title-encoding"
      key="projectdoc.doctype.version.blueprint.form.label.titleEncoding.class.placeholder"/>
    <param name="titleEncoding">#</param>
  </wizard>
</doctype>
