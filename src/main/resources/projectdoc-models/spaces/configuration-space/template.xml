<!--

    Copyright 2013-2025 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<ac:layout>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-properties-marker">
        <ac:parameter ac:name="doctype">index-space</ac:parameter>
        <ac:parameter ac:name="render-as">definition-list</ac:parameter>
        <ac:parameter ac:name="extract-short-desc">true</ac:parameter>
        <ac:rich-text-body>
          <div class="table-wrap">
            <table class="confluenceTable">
              <tbody>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortDescription" /></th>
                  <td class="confluenceTd"><at:var at:name="projectdoc.doctype.common.shortDescription.context.create-space" at:rawxhtml="true" /></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.name" /></th>
                  <td class="confluenceTd">
                    <at:var at:name="name" />
                    <at:var at:name="projectdoc_doctype_common_name" />
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.tags" /></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-tag-list-macro">
                      <ac:parameter ac:name="property">
                        <at:i18n at:key="projectdoc.doctype.common.tags" />
                      </ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.spaceTags" /></th>
                  <td class="confluenceTd">
                    <ac:structured-macro ac:name="projectdoc-display-space-attribute-macro">
                      <ac:parameter ac:name="attribute">labels</ac:parameter>
                    </ac:structured-macro>
                  </td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.flags"/></th>
                  <td class="confluenceTd">projectdoc.space-home, projectdoc.all, projectdoc.index-space</td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh">default-space-closure</th>
                  <td class="confluenceTd">search-space</td>
                  <td class="confluenceTd">hide, space-local</td>
                </tr>
                <tr>
                  <th class="confluenceTh">search-space-local</th>
                  <td class="confluenceTd"></td>
                  <td class="confluenceTd">hide, space-local</td>
                </tr>
                <tr>
                  <th class="confluenceTh">delegate-space</th>
                  <td class="confluenceTd"></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sortKey" /></th>
                  <td class="confluenceTd">
                    <ac:placeholder>
                      <at:i18n at:key="projectdoc.doctype.common.sortKey.placeholder" />
                    </ac:placeholder>
                  </td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh">documentation-json-uri</th>
                  <td class="confluenceTd">https://www.smartics.eu/confluence/download/attachments/12156954/docmap.json?api=v2</td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh">projectdoc.projectdoc-table-merger-macro.param.discard.activate.no-items</th>
                  <td class="confluenceTd">false</td>
                  <td class="confluenceTd">hide</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>

  <ac:layout-section ac:type="two_right_sidebar">
    <ac:layout-cell>
    </ac:layout-cell>
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="borderStyle">none</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="livesearch">
            <ac:parameter ac:name="additional">page excerpt</ac:parameter>
            <ac:parameter ac:name="placeholder"><at:i18n at:key="projectdoc.home.label.search"/></ac:parameter>
            <ac:parameter ac:name="size">large</ac:parameter>
            <ac:parameter ac:name="spaceKey"><at:var at:name="spaceKeyElement" at:rawxhtml="true"/></ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro
        ac:name="projectdoc-section">
        <ac:parameter ac:name="level">*</ac:parameter>
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.space.index.content.quicklinks"/></ac:parameter>
        <ac:rich-text-body>
          <p>
            <ac:structured-macro
              ac:name="projectdoc-space-list-macro">
              <ac:parameter ac:name="categories-connector">true</ac:parameter>
              <ac:parameter ac:name="categories">attachment-space</ac:parameter>
              <ac:parameter ac:name="type">definition-list</ac:parameter>
            </ac:structured-macro>
          </p>
          <p>
            <ac:structured-macro
              ac:name="projectdoc-display-list">
              <ac:parameter ac:name="doctype">docsection</ac:parameter>
              <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.common.doctype"/>&gt;=[docsection] NOT $&lt;<at:i18n at:key="projectdoc.doctype.common.tags"/>&gt;=[doctype-index]</ac:parameter>
              <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            </ac:structured-macro>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
</ac:layout>
