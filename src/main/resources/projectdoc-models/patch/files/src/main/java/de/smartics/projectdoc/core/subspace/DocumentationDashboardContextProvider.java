/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.subspace;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.atlassian.confluence.tools.document.subordinate.DocumentSpecification;
import de.smartics.projectdoc.atlassian.confluence.tools.document.subordinate.SubdocumentsSpecification;
import de.smartics.projectdoc.confluence.tool.blueprint.partition.PartitionContextProviderSupportService;

public class DocumentationDashboardContextProvider
    extends SubspaceContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DocumentationDashboardContextProvider(
      final PartitionContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------
  // --- business -------------------------------------------------------------

  @Override
  public BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    createSubdocumentsSpecification(blueprintContext);
    return super.updateBlueprintContext(blueprintContext);
  }

  private void createSubdocumentsSpecification(
      final BlueprintContext blueprintContext) {
    final SubdocumentsSpecification specification =
        new SubdocumentsSpecification();
    addSubdocument(specification, "fault");
    addSubdocument(specification, "feedback");
    addSubdocument(specification, "pending");
    blueprintContext.put(SubdocumentsSpecification.KEY, specification);
  }

  private void addSubdocument(final SubdocumentsSpecification specification,
      final String id) {
    final String blueprintKey = "de.smartics.atlassian.confluence" +
                                ".smartics-projectdoc-confluence-space-core" +
                                ":de.smartics.projectdoc" +
                                ".core.subspace-core-specialpage-blueprint";
    final String templateLabel = "dashboard";
    final String templateKey = "de.smartics.atlassian.confluence" +
                               ".smartics-projectdoc-confluence-space-core" +
                               ":projectdoc-documentation" +
                               "-dashboard-" + id + "-box-template";
    final String nameKey =
        "projectdoc.content.doctools.documentation-dashboard." + id +
        "-box.name";
    final String shortDescriptionKey =
        "projectdoc.content.doctools.documentation-dashboard." + id +
        "-box.short-description";
    final DocumentSpecification documentSpecification =
        new DocumentSpecification(blueprintKey, templateKey, templateLabel,
            nameKey, shortDescriptionKey, "true");
    specification.add(documentSpecification);
  }

  // --- object basics --------------------------------------------------------

}
