require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
  "use strict";

  AJS.bind("blueprint.wizard-register.ready", function () {
    Confluence.Blueprint
      .setWizard(
        'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-quote',
        function (wizard) {
          wizard.on('pre-render.page1Id', function (e, state) {
            state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
          });

          wizard.on('submit.page1Id', function (e, state) {
            const name = state.pageData["projectdoc_doctype_common_name"];
            if (!name) {
              alert('Please provide context for the quote.');
              return false;
            }

            const shortDescription = state.pageData["projectdoc_doctype_common_shortDescription"];
            if (!shortDescription) {
              alert('Please provide the text of the quote.');
              return false;
            }

            PROJECTDOC.adjustToLocation(state);
          });

          wizard.on('post-render.page1Id', function (e, state) {
            var title = state.wizardData.title;
            if (title) {
              AJS.$('#projectdoc_doctype_common_name').val(title);
              AJS.$('#projectdoc_doctype_common_shortDescription').focus();
            }
            wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
          });
        });
  });
});
