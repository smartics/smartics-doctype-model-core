/*
 * Copyright 2013-2017 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.services;

import de.smartics.projectdoc.core.services.projects.CoreProjectCreationServiceAccessor;

import com.atlassian.confluence.event.events.plugin.PluginDisableEvent;
import com.atlassian.event.api.EventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listens to plugin events to update references to extension services.
 */
public class ExtensionLifecycleListener {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * Reference to the logger for this class.
   */
  private static final Logger LOG =
      LoggerFactory.getLogger(ExtensionLifecycleListener.class);

  // --- members --------------------------------------------------------------

  private final CoreProjectCreationServiceAccessor serviceAccessor;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public ExtensionLifecycleListener(
      final CoreProjectCreationServiceAccessor serviceAccessor) {
    this.serviceAccessor = serviceAccessor;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @EventListener
  public void disable(final PluginDisableEvent event) {
    final String key = event.getPluginKey();
    if (LOG.isDebugEnabled()) {
      LOG.debug("Disabling plugin " + key);
    }
    serviceAccessor.notifyRemovalOf(key);

//    final Plugin plugin = pluginAccessor.getPlugin(key);
//    serviceAccessor.notifyRemovalOf(plugin.getClassLoader());
  }

  // Does not work with Confluence 5.5
//  @EventListener
//  public void disable(final PluginDisablingEvent event) {
//    final Plugin plugin = event.getPlugin();
//    serviceAccessor.notifyRemovalOf(plugin.getClassLoader());
//  }

  // --- object basics --------------------------------------------------------

}
