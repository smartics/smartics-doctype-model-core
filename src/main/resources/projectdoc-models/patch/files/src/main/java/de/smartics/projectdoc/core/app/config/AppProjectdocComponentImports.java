/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.app.config;

import de.smartics.projectdoc.atlassian.confluence.tools.macro.CoreMacroServices;
import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectCreationServiceAccessor;
import de.smartics.projectdoc.confluence.tool.blueprint.document.CategoryDocumentContextProviderSupportService;
import de.smartics.projectdoc.core.services.ExtensionLifecycleListener;
import de.smartics.projectdoc.core.services.projects.CoreProjectCreationServiceAccessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class AppProjectdocComponentImports {

  // ... Creation Service Tracking

  //   <component
  //    key="projectdocCoreProjectCreationServiceAccessor"
  //    name="projectdoc Core Project Creation Service Accessor"
  //    class="de.smartics.projectdoc.core.services.projects
  //    .CoreProjectCreationServiceAccessor">
  //    <interface>de.smartics.projectdoc.atlassian.confluence.tools.projects
  //    .ProjectCreationServiceAccessor</interface>
  //  </component>
  //  <listener
  //    key="projectdocExtensionLifecycleListener"
  //    class="de.smartics.projectdoc.core.services
  //    .ExtensionLifecycleListener" />

  @Bean
  public ProjectCreationServiceAccessor projectCreationServiceAccessor() {
    return importOsgiService(CoreProjectCreationServiceAccessor.class);
  }

  //   <listener
  //    key="projectdocExtensionLifecycleListener"
  //    class="de.smartics.projectdoc.core.services
  //    .ExtensionLifecycleListener" />
  @Bean
  public ExtensionLifecycleListener extensionLifecycleListener() {
    return importOsgiService(ExtensionLifecycleListener.class);
  }

  //   <component-import
  //    key="projectdocCoreMacroServices"
  //    name="projectdoc Core Macro Services"
  //    interface="de.smartics.projectdoc.atlassian.confluence.tools.macro
  //    .CoreMacroServices"/>
  @Bean
  public CoreMacroServices coreMacroServices() {
    return importOsgiService(CoreMacroServices.class);
  }

  @Bean
  public CategoryDocumentContextProviderSupportService categoryDocumentContextProviderSupportService() {
    return importOsgiService(
        CategoryDocumentContextProviderSupportService.class);
  }
}
