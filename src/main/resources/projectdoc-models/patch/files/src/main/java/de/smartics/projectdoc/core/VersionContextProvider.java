/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContextKeys;
import com.atlassian.confluence.util.i18n.I18NBean;
import de.smartics.projectdoc.atlassian.confluence.ProjectDocBlueprintContextAdapter;
import de.smartics.projectdoc.atlassian.confluence.ProjectDocContext;
import de.smartics.projectdoc.atlassian.confluence.api.doctypes.Doctype;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectCreationService;
import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectCreationServiceAccessor;
import de.smartics.projectdoc.confluence.tool.blueprint.document.DocumentContextProviderSupportService;
import org.apache.commons.lang3.StringUtils;


/**
 * Provides a default name and short description for versions.
 */
public class VersionContextProvider extends AbstractBlueprintContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final DocumentContextProviderSupportService support;
  private final ProjectCreationServiceAccessor serviceAccessor;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public VersionContextProvider(
      final DocumentContextProviderSupportService support,
      final ProjectCreationServiceAccessor serviceAccessor) {
    super(support.getTemplateRendererHelper());

    this.support = support;
    this.serviceAccessor = serviceAccessor;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Provides properties to the blueprint context.
   * <p>
   * Makes sure that the optional name and short description is set, if the
   * project model has been set.
   * </p>
   */
  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final BlueprintContext parentContext =
        support.updateBlueprintContext(blueprintContext);
    final ProjectCreationService service = serviceAccessor.getService();
    if (service != null) {
      final ProjectDocContext context =
          new ProjectDocBlueprintContextAdapter(parentContext);
      service.updateVersionBlueprintContext(context);

      final Object unversionedTitleRequested =
          context.get("projectdoc-create-unversionedTitle");
      if ("true".equals(unversionedTitleRequested)) {
        final ContextProviderSupportService accessor = support.getSupport();
        final I18NBean i18n = accessor.getUser().createI18n();
        final String name =
            i18n.getText("projectdoc.doctype.version.versionInformation");
        parentContext.put("projectdoc_doctype_version_versionInformation",
            "<ac:link><ri:page ri:content-title=\"" + name + "\" /></ac:link>");

        if (null == accessor.getPageManager()
            .getPage(parentContext.getSpaceKey(), name)) {
          parentContext.setTitle(name);
          parentContext.put(BlueprintContextKeys.CONTENT_PAGE_TITLE.key(),
              name);
          parentContext.put("projectdoc_doctype_common_title", name);
          parentContext.put("title", name);
        }
      }
    } else {
      final String originalName = (String) parentContext.get(Doctype.NAME);
      final String name = "N/A";
      if (StringUtils.isBlank(originalName)) {
        parentContext.setTitle(name);
        parentContext.put(Doctype.NAME, name);
      }
      final String originalShortDescription =
          (String) parentContext.get(Doctype.SHORT_DESCRIPTION);
      if (StringUtils.isBlank(originalShortDescription)) {
        final ContextProviderSupportService accessor = support.getSupport();
        final I18NBean i18n = accessor.getUser().createI18n();
        final String shortDescription =
            i18n.getText("projectdoc.doctype.version.shortDescription.standard",
                new Object[] {name});
        parentContext.put(Doctype.SHORT_DESCRIPTION, shortDescription);
      }
    }

    support.updateContextFinally(parentContext);

    return parentContext;
  }

  // --- object basics --------------------------------------------------------

}
