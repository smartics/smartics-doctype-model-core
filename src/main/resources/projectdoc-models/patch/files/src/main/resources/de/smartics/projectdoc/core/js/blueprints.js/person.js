require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
  "use strict";

  AJS.bind("blueprint.wizard-register.ready", function () {
    Confluence.Blueprint
      .setWizard(
        'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-person',
        function (wizard) {
          wizard.on('pre-render.page1Id', function (e, state) {
            state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
          });

          wizard.on('submit.page1Id', function (e, state) {
            const givenName = state.pageData["projectdoc_doctype_person_name_givenName"];
            if (!givenName) {
              alert('Please provide a given name for this person document.');
              return false;
            }

            const familyName = state.pageData["projectdoc_doctype_person_name_familyName"];
            if (!familyName) {
              alert('Please provide a family name for this person document.');
              return false;
            }

            const shortDescription = state.pageData["projectdoc_doctype_common_shortDescription"];
            if (!shortDescription) {
              alert('Please provide a short description for this person.');
              return false;
            }

            state.pageData["projectdoc_doctype_common_name"] = givenName + " " + familyName;

            PROJECTDOC.adjustToLocation(state);
          });
          wizard.on('post-render.page1Id', function (e, state) {
            const title = state.wizardData.title;
            if (title) {
              AJS.$('#projectdoc_doctype_common_name').val(title);
              AJS.$('#projectdoc_doctype_common_shortDescription').focus();
            }
          });

          wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
        });
  });
});