package de.smartics.projectdoc.core.services.projects;

import de.smartics.projectdoc.atlassian.confluence.tools.service.project.ServiceDescriptor;

/**
 *
 */
public class PomCreationServiceDescriptor extends ServiceDescriptor {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The key of the module the service is part of.
   */
  private static final String MODULE_KEY =
      "de.smartics.atlassian.smartics-projectdoc-maven-extension";

  /**
   * The name of the service class implementation.
   */
  private static final String SERVICE_CLASS =
      "de.smartics.projectdoc.extension.maven.projects.task.PomCreationService";


  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PomCreationServiceDescriptor() {
    super(MODULE_KEY, SERVICE_CLASS);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
