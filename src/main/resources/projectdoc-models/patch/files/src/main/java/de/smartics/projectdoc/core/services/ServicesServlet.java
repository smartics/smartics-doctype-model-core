/*
 * Copyright 2013-2017 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.services;

import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectCreationService;
import de.smartics.projectdoc.core.services.projects.CoreProjectCreationServiceAccessor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides access on available services.
 *
 * @see http://localhost:1990/confluence/plugins/servlet/projectdoc/core/
 *      services-index?type=ProjectCreationService
 */
public class ServicesServlet extends HttpServlet {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The class version identifier.
   */
  private static final long serialVersionUID = 1L;

  // --- members --------------------------------------------------------------

  private final CoreProjectCreationServiceAccessor serviceAccessor;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public ServicesServlet(
      final CoreProjectCreationServiceAccessor serviceAccessor) {
    this.serviceAccessor = serviceAccessor;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected void doGet(final HttpServletRequest request,
      final HttpServletResponse response) throws ServletException, IOException {
    final String type = request.getParameter("type");

    if ((ProjectCreationService.class.getName().equals(type)
        || "ProjectCreationService".equals(type))
        && null != serviceAccessor.getService()) {
      response.setStatus(HttpServletResponse.SC_OK);
      return;
    }

    response.sendError(HttpServletResponse.SC_NOT_FOUND);
  }

  // --- object basics --------------------------------------------------------

}
