/*
 * Copyright 2013-2017 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.confluence.tool.blueprint.document.CategoryDocumentContextProviderSupportService;

/**
 * Extends for injection.
 */
public class CategoryContextProvider
    extends AbstractBlueprintContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final CategoryDocumentContextProviderSupportService support;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public CategoryContextProvider(
      final CategoryDocumentContextProviderSupportService support) {
    super(support.getTemplateRendererHelper());
    this.support = support;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    return support.updateBlueprintContext(blueprintContext);
  }

  // --- object basics --------------------------------------------------------

}
