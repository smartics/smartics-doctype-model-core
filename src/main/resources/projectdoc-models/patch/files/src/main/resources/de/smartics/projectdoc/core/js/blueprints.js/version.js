require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
  "use strict";

  AJS.bind("blueprint.wizard-register.ready", function () {
    Confluence.Blueprint
      .setWizard(
        'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-version',
        function (wizard) {
          wizard.on('submit.page1Id', function (e, state) {
            let index;
            const pom = state.pageData["projectdoc.doctype.pom.reference"];
            const name = state.pageData["projectdoc_doctype_common_name"];
            if (!pom) {
              if (!name) {
                alert('Please provide a POM or an ID for this version.');
                return false;
              }

              var shortDescription = state.pageData["projectdoc_doctype_common_shortDescription"];
              if (!shortDescription) {
                alert('Please provide a short description for the document.');
                return false;
              }
            }

            if (!name) {
              var version = "N/A";
              if (pom.endsWith(".pom")) {
                index = pom.lastIndexOf("-");
                if (index < pom.length - 5) {
                  version = 'V' + pom.substring(index + 1, pom.length - 4);
                }
              } else {
                index = pom.lastIndexOf(":");
                if (index < pom.length - 1) {
                  version = 'V' + pom.substring(index + 1);
                }
              }
              state.pageData["projectdoc_doctype_common_name"] = version;
            }

            PROJECTDOC.adjustToLocation(state);
          });

          wizard.on('pre-render.page1Id', function (e, state) {
            state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);

            const url = AJS.contextPath() + "/plugins/servlet/projectdoc/core/services-index?type=ProjectCreationService";
            state.soyRenderContext['pomReferenceFieldStatus'] = 'disabled';
            state.soyRenderContext['pomReferenceFieldTitle'] = AJS.I18n.getText("projectdoc.doctype.version.blueprint.wizard.pomReferenceField.title.disabled");
            AJS.$.ajax({
              url: url,
              success: function (data, status) {
                if (status === 'success') {
                  state.soyRenderContext['pomReferenceFieldStatus'] = ' ';
                  state.soyRenderContext['pomReferenceFieldTitle'] = AJS.I18n.getText("projectdoc.doctype.version.blueprint.wizard.pomReferenceField.title.enabled");
                }
              },
              async: false
            });
          });

          wizard.on('post-render.page1Id', function (e, state) {
            const title = state.wizardData.title;
            if (title) {
              AJS.$('#projectdoc_doctype_common_name').val(title);
              AJS.$('#projectdoc_doctype_common_shortDescription').focus();
            }
            wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
          });
        });
  });
})