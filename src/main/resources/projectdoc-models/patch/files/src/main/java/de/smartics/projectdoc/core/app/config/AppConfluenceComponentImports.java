/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.app.config;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class AppConfluenceComponentImports {

  // ... Install Servlet

  //   <component-import
  //    key="userManager"
  //    name="Atlassian SAL User Manager"
  //    interface="com.atlassian.sal.api.user.UserManager" />
  //  <component-import
  //    key="loginUriProvider"
  //    name="Atlassian SAL Login URI Provider"
  //    interface="com.atlassian.sal.api.auth.LoginUriProvider" />

  @Bean
  public UserManager userManager() {
    return importOsgiService(UserManager.class);
  }

  @Bean
  public LoginUriProvider loginUriProvider() {
    return importOsgiService(LoginUriProvider.class);
  }

  @Bean
  public SpaceManager spaceManager() {
    return importOsgiService(SpaceManager.class);
  }

  @Bean
  public PageManager pageManager() {
    return importOsgiService(PageManager.class);
  }

  @Bean
  public LocaleManager localeManager() {
    return importOsgiService(LocaleManager.class);
  }

  @Bean
  public I18NBeanFactory i18NBeanFactory() {
    return importOsgiService(I18NBeanFactory.class);
  }


  @Bean
  public ContentPropertyManager contentPropertyManager() {
    return importOsgiService(ContentPropertyManager.class);
  }
}
