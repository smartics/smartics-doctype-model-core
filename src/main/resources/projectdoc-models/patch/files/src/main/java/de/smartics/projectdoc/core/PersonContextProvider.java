/*
 * Copyright 2013-2017 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.BlueprintContextNames;
import de.smartics.projectdoc.confluence.tool.blueprint.document.DocumentContextProviderSupportService;

/**
 * Provides a default name and short description for versions.
 */
public class PersonContextProvider
    extends AbstractProjectDocContextProviderExt {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The given name of the person as specified in the wizard to create a
   * document of type 'Person'.
   * <p>
   * The value of this constant is {@value}.
   * </p>
   */
  private static final String KEY_GIVEN_NAME =
      "projectdoc_doctype_person_name_givenName";

  /**
   * The family name of the person as specified in the wizard to create a
   * document of type 'Person'.
   * <p>
   * The value of this constant is {@value}.
   * </p>
   */
  private static final String KEY_FAMILY_NAME =
      "projectdoc_doctype_person_name_familyName";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PersonContextProvider(
      final DocumentContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Provides properties to the blueprint context.
   * <p>
   * Constructs the document name from the given and family name of the person.
   * </p>
   */
  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final String givenName = (String) blueprintContext.get(KEY_GIVEN_NAME);
    final String familyName = (String) blueprintContext.get(KEY_FAMILY_NAME);
    final String documentName = givenName + ' ' + familyName;
    blueprintContext.put(BlueprintContextNames.NAME, documentName);

    return super.updateBlueprintContext(blueprintContext);
  }

  // --- object basics --------------------------------------------------------

}
