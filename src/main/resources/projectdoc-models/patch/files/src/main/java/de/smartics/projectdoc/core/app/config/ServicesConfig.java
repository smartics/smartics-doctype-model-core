/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.app.config;


import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createcontent.ContentBlueprintManager;
import com.atlassian.confluence.plugins.createcontent.api.services.ContentBlueprintService;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import de.smartics.projectdoc.atlassian.confluence.persistence.ProjectdocMarkerSupport;
import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectCreationServiceAccessor;
import de.smartics.projectdoc.confluence.tool.blueprint.document.CategoryDocumentContextProviderSupportService;
import de.smartics.projectdoc.confluence.tool.blueprint.document.DocumentContextProviderSupportService;
import de.smartics.projectdoc.confluence.tool.blueprint.partition.PartitionContextProviderSupportService;
import de.smartics.projectdoc.core.CategoryContextProvider;
import de.smartics.projectdoc.core.PersonContextProvider;
import de.smartics.projectdoc.core.ProjectDocContextProviderExt;
import de.smartics.projectdoc.core.VersionContextProvider;
import de.smartics.projectdoc.core.services.projects.CoreProjectCreationServiceAccessor;
import de.smartics.projectdoc.core.subspace.DocumentationDashboardContextProvider;
import de.smartics.projectdoc.core.subspace.PartitionContextProvider;
import de.smartics.projectdoc.core.subspace.SubspaceContextProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;

@Configuration
@Import({ConfluenceComponentImports.class, ProjectdocComponentImports.class,
    AppConfluenceComponentImports.class, AppProjectdocComponentImports.class})
public class ServicesConfig {
  //   <component
  //    key="projectdocCoreProjectCreationServiceAccessor"
  //    name="projectdoc Core Project Creation Service Accessor"
  //    class="de.smartics.projectdoc.core.services.projects
  //    .CoreProjectCreationServiceAccessor">
  //    <interface>de.smartics.projectdoc.atlassian.confluence.tools.projects
  //    .ProjectCreationServiceAccessor</interface>
  //  </component>
  @Bean
  public ProjectCreationServiceAccessor projectCreationServiceAccessor(
      final ApplicationContext applicationContext) {
    return new CoreProjectCreationServiceAccessor(applicationContext);
  }

  @Bean
  public SubspaceContextProvider subspaceContextProvider(
      final PartitionContextProviderSupportService support) {
    return new SubspaceContextProvider(support);
  }

  @Bean
  public PartitionContextProvider PartitionContextProvider(
      final ContextProviderSupportService support) {
    return new PartitionContextProvider(support);
  }

  @Bean
  public ProjectDocContextProviderExt projectDocContextProviderExt(
      final DocumentContextProviderSupportService support) {
    return new ProjectDocContextProviderExt(support);
  }

  @Bean
  public CategoryContextProvider categoryContextProvider(
      final CategoryDocumentContextProviderSupportService support) {
    return new CategoryContextProvider(support);
  }

  @Bean
  public PersonContextProvider personContextProvider(
      final DocumentContextProviderSupportService support) {
    return new PersonContextProvider(support);
  }

  @Bean
  public VersionContextProvider versionContextProvider(
      final DocumentContextProviderSupportService support,
      final ProjectCreationServiceAccessor serviceAccessor) {
    return new VersionContextProvider(support, serviceAccessor);
  }

  @Bean
  public DocumentationDashboardContextProvider documentationDashboardContextProvider(
      final PartitionContextProviderSupportService support) {
    return new DocumentationDashboardContextProvider(support);
  }
}
