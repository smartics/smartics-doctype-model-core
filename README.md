# Core Doctypes


## Overview

This project specifies a model to create a free [doctype add-on](https://www.smartics.eu/confluence/x/nQHJAw)
for the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp)
for [Confluence](https://www.atlassian.com/software/confluence).

The add-on provides base blueprints for project documentation, including

  * Organize Information
    * Glossary Item
    * FAQ
    * Resource
    * Quote
  * Identify People
    * Stakeholder
    * Person
    * Organization
  * Modularize Content
    * Module
    * Section
    * Space Index
    * Step
    * Topic
    * Tour
  * Categorize Content
    * Role
    * Tag
    * Category
    * Subject
  * and [many more](https://www.smartics.eu/confluence/x/iApwAg)!


## Fork me!
Feel free to fork this project to adjust the model according to your project
requirements.

The Core Doctypes Model and it's derived artifacts are licensed under
[Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/x/iApwAg)
  * [projectdoc Toolbox Online Manual](https://www.smartics.eu/confluence/x/EAFk)
  * [Doctype Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw)
